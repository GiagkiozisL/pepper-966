package com.cowabunga.pepperradio.model;

import android.os.Parcel;
import android.os.Parcelable;


public class Track implements Parcelable {


    public String trackId; // artist_title
    public String title = "";
    public String artist = "";
    public String album = "";
    public int duration = 0;
    public String startDate; // 2020-03-09T08:04:00
    public long timestamp;


    public Track(Parcel source) {
        readFromParcel(source);
    }

    public Track() {}

    public static final Creator<Track> CREATOR =
            new Creator<Track>() {
                @Override
                public Track createFromParcel(Parcel source) { return new Track(source);}

                @Override
                public Track[] newArray(int size) { return new Track[size];}
            };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(trackId);
        dest.writeString(title);
        dest.writeString(artist);
        dest.writeString(album);
        dest.writeInt(duration);
        dest.writeString(startDate);
        dest.writeLong(timestamp);
    }

    private void readFromParcel(Parcel source) {
        trackId = source.readString();
        title = source.readString();
        artist = source.readString();
        album = source.readString();
        duration = source.readInt();
        startDate = source.readString();
        timestamp = source.readLong();
    }
}
