package com.cowabunga.pepperradio.model;

import android.os.Parcel;
import android.os.Parcelable;


public class Producer implements Parcelable {

    public String id = "";
    public String name = "";
    public String workingHours = "";
    public String imageUrl = "";
    public String infoUrl = "";
    public int onlineUsers = 50;

    public Producer() {}


    public Producer(Parcel source) {
        readFromParcel(source);
    }

    public void setImageUrl(String mImageUri) {
        this.imageUrl = mImageUri;
    }

    public static final Creator<Producer> CREATOR =
            new Creator<Producer>() {
                @Override
                public Producer createFromParcel(Parcel source) { return new Producer(source);}

                @Override
                public Producer[] newArray(int size) { return new Producer[size];}
            };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(workingHours);
        dest.writeString(imageUrl);
        dest.writeString(infoUrl);
        dest.writeInt(onlineUsers);
    }

    private void readFromParcel(Parcel source) {
        id = source.readString();
        name = source.readString();
        workingHours = source.readString();
        imageUrl = source.readString();
        infoUrl = source.readString();
        infoUrl = source.readString();
        onlineUsers = source.readInt();
    }
}
