package com.cowabunga.pepperradio.db.tables;

import android.database.sqlite.SQLiteDatabase;

public class ProducersTable {

    public static final String TABLE_Producers = "producers";
    public static final String COLUMN_ID = "_id";
    public static final String column_producer_id = "producer_id";
    public static final String column_name = "name";
    public static final String column_working_hours = "working_hours";
    public static final String column_image_url = "image_url";
    public static final String column_online_users = "online_users";
    public static final String column_info_url = "info_url";


    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_Producers
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + column_producer_id + " text, "
            + column_name + " text, "
            + column_working_hours + " text, "
            + column_online_users + " int, "
            + column_image_url + " text, "
            + column_info_url + " text"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }
}
