package com.cowabunga.pepperradio.db.queries;

import android.content.Context;
import android.database.Cursor;

import com.cowabunga.pepperradio.db.PPContentProvider;
import com.cowabunga.pepperradio.db.tables.FavoriteTracksTable;
import com.cowabunga.pepperradio.db.tables.ProducersTable;
import com.cowabunga.pepperradio.model.Producer;
import com.cowabunga.pepperradio.model.Track;

import java.util.ArrayList;
import java.util.List;


public class PPDBQueries {

    public static Track parseTrackFromCursor(Cursor cursor) {
        Track track = new Track();

        int id = cursor.getInt(cursor.getColumnIndexOrThrow(FavoriteTracksTable.COLUMN_ID));
        track.title = cursor.getString(cursor.getColumnIndexOrThrow(FavoriteTracksTable.COLUMN_TITLE));
        track.trackId = cursor.getString(cursor.getColumnIndexOrThrow(FavoriteTracksTable.COLUMN_track_ID));
        track.album = cursor.getString(cursor.getColumnIndexOrThrow(FavoriteTracksTable.COLUMN_ALBUM));
        track.artist = cursor.getString(cursor.getColumnIndexOrThrow(FavoriteTracksTable.COLUMN_ARTIST));
        track.timestamp = cursor.getLong(cursor.getColumnIndexOrThrow(FavoriteTracksTable.COLUMN_TIMESTAMP));
        return track;
    }

    public static List<Producer> queryAllProducers(Context ctx) {

        List<Producer> producerList = new ArrayList<>();
        Producer producer = null;
        String[] projection = PPContentProvider.PRODUCER_PROJECTION;
        String selection = null;
        String[] selectionArgs = null;
        String sort = null;
        Cursor cursor = ctx.getContentResolver().query(PPContentProvider.CONTENT_URI_PRODUCERS, projection, selection, selectionArgs, sort);

        if (cursor != null && cursor.moveToFirst()) {
            for (int i = 0; i < cursor.getCount(); i++) {
                try {
                    producer = new Producer();
                    producer.id = cursor.getString(cursor.getColumnIndexOrThrow(ProducersTable.column_producer_id));
                    producer.name = cursor.getString(cursor.getColumnIndexOrThrow(ProducersTable.column_name));
                    producer.workingHours = cursor.getString(cursor.getColumnIndexOrThrow(ProducersTable.column_working_hours));
                    producer.imageUrl = cursor.getString(cursor.getColumnIndexOrThrow(ProducersTable.column_image_url));
                    producer.infoUrl = cursor.getString(cursor.getColumnIndexOrThrow(ProducersTable.column_info_url));

                    producerList.add(producer);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }

        return producerList;
    }

    public static boolean trackIsFavorite(Context ctx, String trackId) {

        boolean exists = false;

        String[] projection = PPContentProvider.FAVORITE_TRACK_PROJECTION;
        String selection = FavoriteTracksTable.COLUMN_track_ID + "=?";
        String[] selectionArgs = new String[]{trackId};
        String sort = null;
        Cursor cursor = ctx.getContentResolver().query(PPContentProvider.CONTENT_URI_FAVORITE_TRACKS, projection, selection, selectionArgs, sort);

        if (cursor != null && cursor.moveToFirst()) {
            try {
                exists = true;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }
        }

        return exists;
    }

//    public static Video queryVideoById(Context ctx, String videoId) {
//
//        Video video = null;
//        String[] projection = DBContentProvider.VIDEO_PROJECTION;
//        String selection = VideosTable.COLUMN_video_id + "=?";
//        String[] selectionArgs = new String[]{videoId};
//        String sort = null;
//        Cursor cursor = ctx.getContentResolver().query(DBContentProvider.CONTENT_URI_VIDEOS, projection, selection, selectionArgs, sort);
//
//        if (cursor != null && cursor.moveToFirst()) {
//            try {
//                video = new Video();
//                video.videoId = cursor.getString(cursor.getColumnIndexOrThrow(VideosTable.COLUMN_video_id));
//                video.urlPath = cursor.getString(cursor.getColumnIndexOrThrow(VideosTable.COLUMN_url_path));
//                video.internalPath = cursor.getString(cursor.getColumnIndexOrThrow(VideosTable.COLUMN_internal_path));
//                video.durationSecs = cursor.getInt(cursor.getColumnIndexOrThrow(VideosTable.COLUMN_duration_secs));
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                cursor.close();
//            }
//        }
//
//        return video;
//    }
}
