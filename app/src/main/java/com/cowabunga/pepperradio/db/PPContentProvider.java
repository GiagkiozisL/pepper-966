package com.cowabunga.pepperradio.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.cowabunga.pepperradio.db.tables.FavoriteTracksTable;
import com.cowabunga.pepperradio.db.tables.ProducersTable;

import java.util.Arrays;
import java.util.HashSet;


public class PPContentProvider extends ContentProvider {

    //    database
    private PPDBHelper mDatabase;

    //    used for the Uri matcher
    private static final int FAVORITE_TRACK = 10;
    private static final int PRODUCER = 30;

    public static final String AUTHORITY = "com.cowabunga.pepperradio.db.PPContentProvider";

    private static final String BASE_PATH_FAVORITE_TRACKS = FavoriteTracksTable.TABLE_FAVORITE_TRACKS;
    private static final String BASE_PATH_PRODUCERS = ProducersTable.TABLE_Producers;

    public static final Uri CONTENT_URI_FAVORITE_TRACKS = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH_FAVORITE_TRACKS);
    public static final Uri CONTENT_URI_PRODUCERS = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH_PRODUCERS);

    public static final String[] FAVORITE_TRACK_PROJECTION = {
            FavoriteTracksTable.COLUMN_ID,
            FavoriteTracksTable.COLUMN_track_ID,
            FavoriteTracksTable.COLUMN_TITLE,
            FavoriteTracksTable.COLUMN_ARTIST,
            FavoriteTracksTable.COLUMN_ALBUM,
            FavoriteTracksTable.COLUMN_TIMESTAMP
    };

    public static final String[] PRODUCER_PROJECTION = {
            ProducersTable.COLUMN_ID,
            ProducersTable.column_producer_id,
            ProducersTable.column_name,
            ProducersTable.column_working_hours,
            ProducersTable.column_online_users,
            ProducersTable.column_image_url,
            ProducersTable.column_info_url
    };

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH_FAVORITE_TRACKS, FAVORITE_TRACK);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH_PRODUCERS, PRODUCER);
    }

    @Override
    public boolean onCreate() {
        mDatabase = new PPDBHelper(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        // Uisng SQLiteQueryBuilder instead of query() method
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setDistinct(true);

        // check if the caller has requested a column which does not exists
        checkColumns(uri, projection);

        // Set the table
        if (uri.compareTo(CONTENT_URI_FAVORITE_TRACKS) == 0) {
            queryBuilder.setTables(FavoriteTracksTable.TABLE_FAVORITE_TRACKS);
        } else if (uri.compareTo(CONTENT_URI_PRODUCERS) == 0) {
            queryBuilder.setTables(ProducersTable.TABLE_Producers);
        }

        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case FAVORITE_TRACK:
                break;
            case PRODUCER:
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = mDatabase.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        // make sure that potential listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = mDatabase.getWritableDatabase();
        long id;
        String basePath;
        switch (uriType) {
            case FAVORITE_TRACK:
                id = sqlDB.insertWithOnConflict(FavoriteTracksTable.TABLE_FAVORITE_TRACKS, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                basePath = BASE_PATH_FAVORITE_TRACKS;
                break;
            case PRODUCER:
                id = sqlDB.insertWithOnConflict(ProducersTable.TABLE_Producers, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                basePath = BASE_PATH_PRODUCERS;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
//        System.out.println("row inserted " + id);
        return Uri.parse(basePath + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = mDatabase.getWritableDatabase();
        int rowsDeleted;
        String id;
        switch (uriType) {
            case FAVORITE_TRACK:
                rowsDeleted = sqlDB.delete(FavoriteTracksTable.TABLE_FAVORITE_TRACKS, selection, selectionArgs);
                break;
            case PRODUCER:
                rowsDeleted = sqlDB.delete(ProducersTable.TABLE_Producers, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = mDatabase.getWritableDatabase();
        int rowsUpdated;
        String id = null;
        switch (uriType) {
            case FAVORITE_TRACK:
                rowsUpdated = sqlDB.update(FavoriteTracksTable.TABLE_FAVORITE_TRACKS,
                        values,
                        selection,
                        selectionArgs);
                break;
            case PRODUCER:
                rowsUpdated = sqlDB.update(ProducersTable.TABLE_Producers,
                        values,
                        selection,
                        selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        System.out.println("row updated " + uri);
        return rowsUpdated;
    }

    private void checkColumns(Uri uri, String[] projection) {

        if (projection != null) {
            HashSet<String> requestedColumns = new HashSet<>(Arrays.asList(projection));
            HashSet<String> availableColumns = createAvailableColumnsHash(uri);
            // check if all columns which are requested are available
            if (!availableColumns.containsAll(requestedColumns)) {
                throw new IllegalArgumentException("Unknown columns in projection");
            }
        }
    }

    private HashSet<String> createAvailableColumnsHash(Uri uri) {
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case FAVORITE_TRACK:
                return new HashSet<>(Arrays.asList(FAVORITE_TRACK_PROJECTION));
            case PRODUCER:
                return new HashSet<>(Arrays.asList(PRODUCER_PROJECTION));
            default:
                return null;
        }
    }
}
