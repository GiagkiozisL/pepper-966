package com.cowabunga.pepperradio.db.tables;

import android.database.sqlite.SQLiteDatabase;


public class FavoriteTracksTable {

    //    Database Table
    public static final String TABLE_FAVORITE_TRACKS = "favoriteTracks";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_track_ID = "_track_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_ARTIST = "artist";
    public static final String COLUMN_ALBUM = "album";
    public static final String COLUMN_TIMESTAMP = "timestamp";


    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_FAVORITE_TRACKS
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_TITLE + " text, "
            + COLUMN_track_ID + " text, "
            + COLUMN_ARTIST + " text, "
            + COLUMN_ALBUM + " text, "
            + COLUMN_TIMESTAMP + " int"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }
}
