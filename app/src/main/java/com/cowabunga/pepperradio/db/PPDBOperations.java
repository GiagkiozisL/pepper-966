package com.cowabunga.pepperradio.db;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.cowabunga.pepperradio.db.tables.FavoriteTracksTable;
import com.cowabunga.pepperradio.db.tables.ProducersTable;
import com.cowabunga.pepperradio.model.Producer;
import com.cowabunga.pepperradio.model.Track;

public class PPDBOperations {

    public static void insertFavoriteTrackToDB(Context context, Track track) {
        if (track == null) {
            return;
        }

        ContentValues values = new ContentValues();
        values.put(FavoriteTracksTable.COLUMN_track_ID, track.trackId);
        values.put(FavoriteTracksTable.COLUMN_ARTIST, track.artist);
        values.put(FavoriteTracksTable.COLUMN_TITLE, track.title);
        values.put(FavoriteTracksTable.COLUMN_ALBUM, track.album);
        values.put(FavoriteTracksTable.COLUMN_TIMESTAMP, System.currentTimeMillis());

        Uri uri = context.getContentResolver().insert(PPContentProvider.CONTENT_URI_FAVORITE_TRACKS, values);
        if (uri != null) {
            Log.i("PPDBOperations", "fav inserted");
        }
    }

    public static void removeFavoriteTrackFromDB(Context context, Track track) {
        String[] selectionArgs = {String.valueOf(track.trackId)};
        context.getContentResolver().delete(PPContentProvider.CONTENT_URI_FAVORITE_TRACKS, FavoriteTracksTable.COLUMN_track_ID + "=?", selectionArgs);
    }

    public static void insertProducerToDB(Context context, Producer producer) {
        ContentValues values = new ContentValues();

        values.put(ProducersTable.column_producer_id, producer.id);
        values.put(ProducersTable.column_name, producer.name);
        values.put(ProducersTable.column_working_hours, producer.workingHours);
        values.put(ProducersTable.column_image_url , producer.imageUrl);
        values.put(ProducersTable.column_info_url, producer.infoUrl);
        values.put(ProducersTable.column_online_users, producer.onlineUsers);

        Uri uri = context.getContentResolver().insert(PPContentProvider.CONTENT_URI_PRODUCERS, values);
        if (uri != null) {
            Log.i("PPDBOperations", "producer inserted");
        }
    }

    public static void updateProducerToDB(Context context, String producerId, Producer producer) {

        String[] selectionArgs = {String.valueOf(producerId)};
        ContentValues values = new ContentValues();
        values.put(ProducersTable.column_producer_id, producer.id);
        values.put(ProducersTable.column_name, producer.name);
        values.put(ProducersTable.column_working_hours, producer.workingHours);
        values.put(ProducersTable.column_online_users, producer.onlineUsers);
        values.put(ProducersTable.column_image_url , producer.imageUrl);
        values.put(ProducersTable.column_info_url, producer.infoUrl);

        context.getContentResolver().update(PPContentProvider.CONTENT_URI_PRODUCERS, values, ProducersTable.column_producer_id + "=?", selectionArgs);
        Uri uri = context.getContentResolver().insert(PPContentProvider.CONTENT_URI_PRODUCERS, values);
        if (uri != null) {
            Log.i("PPDBOperations", "producer updated");
        }
    }

    public static void dropProducers(Context context) {
        context.getContentResolver().delete(PPContentProvider.CONTENT_URI_PRODUCERS, null, null);
    }

    public static void dropFavorites(Context context) {
        context.getContentResolver().delete(PPContentProvider.CONTENT_URI_FAVORITE_TRACKS, null, null);
    }
}
