package com.cowabunga.pepperradio.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.cowabunga.pepperradio.db.tables.FavoriteTracksTable;
import com.cowabunga.pepperradio.db.tables.ProducersTable;

/**
 * Created by akis on 14/07/16.
 */
public class PPDBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "pepperradioplayer.db";
    private static final int DATABASE_VERSION = 1;

    public PPDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        FavoriteTracksTable.onCreate(db);
        ProducersTable.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
