package com.cowabunga.pepperradio.utils;

import android.content.Context;

import com.cowabunga.pepperradio.application.Constants;
import com.cowabunga.pepperradio.application.PreferencesManager;
import com.cowabunga.pepperradio.model.Producer;
import com.cowabunga.pepperradio.model.Track;
import com.cowabunga.pepperradio.utils.RadioPlayerUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JSONParser {


    public static void parseUrls(String jsonStr, Context ctx) {

        PreferencesManager preferencesManager = PreferencesManager.getInstance(ctx);

        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            preferencesManager.setStreamUrl(jsonObject.optString("stream_url"));
            preferencesManager.setNowPlayingUrl(jsonObject.optString("now_playing_url"));
            preferencesManager.setHistoryUrl(jsonObject.optString("history_url"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<Producer> parseProducersFormJson(String jsonStr) {
        List<Producer> producerList = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            JSONArray jsonArray = jsonObject.optJSONArray("djs");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject djObj = jsonArray.optJSONObject(i);
                Producer producer = new Producer();
                producer.id = djObj.optString("id");
                producer.name = djObj.optString("name");
                producer.workingHours = djObj.optJSONArray("working_hours").toString();
                producer.infoUrl = djObj.optString("djInfoUrl");
                producer.imageUrl = djObj.optString("djImageUrl");
                producer.onlineUsers = djObj.optInt("onlineOsers");

                producerList.add(producer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return producerList;
    }

    public static Track parseTrackFromjs(String javascript) {

//         jQuery1101033503685218282575_1583843636369({"album": null, "sku": "", "thumb": "", "artist": "GIZMO VARILLAS", "title": "LOVE OVER EVERYTHING (RADIO EDIT) ", "show": {}, "buy_urls": "", "info_urls": "", "duration": "0", "guid": null});

        Track track = new Track();
        track.trackId = Constants.empty_track_id;
        if (javascript == null || javascript.isEmpty()) {
            return track;
        }

        int jsonindexA = javascript.indexOf("({");
        int jsonindexB = javascript.indexOf("})");

        String jsonString = javascript.substring(jsonindexA + 1, jsonindexB + 1);

        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            track.artist = RadioPlayerUtils.formatCapsName(jsonObject.optString("artist"));
            track.title = RadioPlayerUtils.formatCapsName(jsonObject.optString("title"));
            track.trackId = track.artist + "_" + track.title;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return track;

    }
}
