package com.cowabunga.pepperradio.utils;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.telephony.TelephonyManager;
import android.text.format.DateUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;


public class RadioPlayerUtils {

    /**
     *     mon -> 0 - 24
     *     thu -> 25 - 48
     *     wed -> 59 - 72
     *     tue -> 73 - 96
     *     fri -> 97 - 120
     *     sat -> 121 - 144
     *     sun -> 145 - 168
     *
     * */

    private static final int J_lo_ID = 21;

    private static Map<String, Integer> weekMap = new HashMap<>();

    static {
        weekMap.put("mon", 0);
        weekMap.put("tue", 1);
        weekMap.put("wed", 2);
        weekMap.put("thu", 3);
        weekMap.put("fri", 4);
        weekMap.put("sat", 5);
        weekMap.put("sun", 6);
    }

    public static int findHourTimeInWeek() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        String weekDay = RadioPlayerUtils.getWeekDays().get(day - 1);
        int hourOfDay = getLocalHour();

        int offset = 24 * weekMap.get(weekDay);
        return offset + hourOfDay;
    }


    public static ArrayList<String> getWeekDays() {

        ArrayList<String> weekDays = new ArrayList<>();
        String[] namesOfDays = new String[]{"sun", "mon", "tue", "wed", "thu", "fri", "sat"};

        for (int i = 0; i < 7; i++) {
            weekDays.add(namesOfDays[i]);
        }

        return weekDays;
    }

    public static int convertDpToPixels(Context context, int dp) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    public static String getTimestampString(long timeDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeDate);

        boolean isToday = DateUtils.isToday(timeDate);

        int minute = calendar.get(Calendar.MINUTE);
        String minuteString = String.valueOf(minute).length() == 1 ? "0" + minute : String.valueOf(minute);
        String hourString = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
        String monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
        String dateString = calendar.get(Calendar.DAY_OF_MONTH) + "/" + monthString + "/" + String.valueOf(calendar.get(Calendar.YEAR)).substring(2, 4);

        if (isToday) {
            return  hourString + ":" + minuteString;
        } else {
            return dateString + ", " + hourString + ":" + minuteString;
        }
    }

    public static String getTimestampDigitalType(long timeDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeDate);

        int minute = calendar.get(Calendar.MINUTE);
        String minuteString = String.valueOf(minute).length() == 1 ? "0" + minute : String.valueOf(minute);
        String hourString = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
        int second = calendar.get(Calendar.SECOND);
        String secondString = String.valueOf(second).length() == 1 ? "0" + second : String.valueOf(second);

        return hourString + ":" + minuteString + ":" + secondString;
    }

    public static int getLocalHour() {

        Calendar c = Calendar.getInstance();
        System.out.println("current: " + c.getTime());

        TimeZone timeZone = c.getTimeZone();

        if (!timeZone.getID().equalsIgnoreCase("Europe/Athens")) {
            TimeZone athensTimezone = TimeZone.getTimeZone("Europe/Athens");
            c.setTimeZone(athensTimezone);
        }

        return c.get(Calendar.HOUR_OF_DAY);
    }

    public static boolean timeHourHasChanged() {
        Calendar calendar = Calendar.getInstance();
        int minute = calendar.get(Calendar.MINUTE);
        return minute == 0;
    }

    public static byte[] strToByt(String hexString) {
        int len = hexString.length()/2;
        byte[] result = new byte[len];
        for (int i = 0; i < len; i++) {
            result[i] = Integer.valueOf(hexString.substring(2*i, 2*i+2), 16).byteValue();
        }
        return result;
    }


    @RequiresApi(21)
    private static boolean isJobBreathing(JobScheduler jobScheduler) {

        boolean hasBeenScheduled = false;

        for (JobInfo jobInfo : jobScheduler.getAllPendingJobs()) {
            if ( jobInfo.getId() == J_lo_ID) {
                hasBeenScheduled = true;
                break;
            }
        }

        return hasBeenScheduled;
    }

//    public static void callBatman(Context ctx, String flag) {
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            startDTService(ctx, flag);
//        } else {
//            Intent serviceIntent = new Intent(ctx, BatService.class);
//            serviceIntent.putExtra("SRC", flag);
//            ctx.startService(serviceIntent);
//        }
//    }
//
//    @RequiresApi(21)
//    private static void startDTService(Context ctx, String flag) {
//
//        try {
//            JobScheduler jobScheduler = (JobScheduler) ctx.getSystemService(Context.JOB_SCHEDULER_SERVICE);
//            if (jobScheduler == null) { return; }
//
//            if (isJobBreathing(jobScheduler)) { return; }
//
//            PersistableBundle bundle = new PersistableBundle();
//            bundle.putString("SRC", flag);
//            ComponentName componentName = new ComponentName(ctx, BatJService.class);
//            JobInfo jobInfo = new JobInfo.Builder(J_lo_ID, componentName)
//                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
//                    .setRequiresCharging(false)
//                    .setBackoffCriteria(30000, JobInfo.BACKOFF_POLICY_LINEAR)
//                    .setRequiresDeviceIdle(false)
//                    .setExtras(bundle)
//                    .build();
//
//            jobScheduler.schedule(jobInfo);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


    public static boolean isO() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    }

    public static int getRandomInt(int min, int max) {
        return new Random().nextInt(max - min) + min;
    }

    public static String formatCapsName(String capitalizedWord) {

        if (capitalizedWord.contains("  ")) {
            capitalizedWord = capitalizedWord.replaceAll("  ", " ");
        }

        if (capitalizedWord.contains(" ")) {
            String[] parts  = capitalizedWord.split(" ");

            StringBuilder stringBuilder = new StringBuilder();
            try {
                int count = 0;
                for (String part : parts) {
                    count++;
                    String formatted = part.substring(0, 1).toUpperCase() + part.substring(1).toLowerCase();
                    stringBuilder.append(formatted);

                    if (count < parts.length) {
                        stringBuilder.append(" ");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                stringBuilder.append(capitalizedWord);
            }
            return stringBuilder.toString();
        } else {
            try {
                return capitalizedWord.substring(0, 1).toUpperCase() + capitalizedWord.substring(1).toLowerCase();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return capitalizedWord;

    }

    public static long getTimestampFromDate(String strdate) { // 2020-03-09T08:04:00

        try {
            String format = "yyyy-MM-DD'T'HH:mm:SS";
            DateFormat formatter = new SimpleDateFormat(format);
            TimeZone athensTimezone = TimeZone.getTimeZone("Europe/Athens");
            formatter.setTimeZone(athensTimezone);
            Date date = formatter.parse(strdate);

            return date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }
}
