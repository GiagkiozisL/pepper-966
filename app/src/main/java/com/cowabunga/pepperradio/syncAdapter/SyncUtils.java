package com.cowabunga.pepperradio.syncAdapter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import com.cowabunga.pepperradio.R;
import com.cowabunga.pepperradio.application.PreferencesManager;

import java.util.concurrent.TimeUnit;




public class SyncUtils {

    private static final long SYNC_FREQUENCY = TimeUnit.MINUTES.toSeconds(30); //TimeUnit.HOURS.toSeconds(1);  // 24 hours (in seconds)

    private static PreferencesManager mPreferences;

    private static String mAccountType = "";
    private static String mAccountName = "";
    private static String mContentAuthority = "";

    public static final String SYNC_IDENTIFICATION_EXTRA = "syncIdentifier:";

//    public static void initSyncingAccount(Context context, String src) {
//
//        String type = "";
//        String aName = "";
//        String auth = "";
//
//        SBPreferences prefs = SBPreferences.getInstance(context);
//
//        boolean newAccount = false;
//        boolean setupCompleted = prefs.getSyncCompleted();
//
//        try {
//            ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
//            Bundle bundle = ai.metaData;
//            auth = bundle.getString(MANIFEST_AUTHORITY);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        aName = context.getResources().getString(R.string.sb_account_a);
//        type = context.getResources().getString(R.string.sb_account_t);
//
//        Account account = SyncAService.getAccount(aName, type);
//        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
//
//        if (accountManager != null && accountManager.addAccountExplicitly(account, null, null) && !auth.isEmpty()) {
//            try {
//                ContentResolver.setIsSyncable(account, auth, 1);
//                ContentResolver.setSyncAutomatically(account, auth, true);
//                Bundle b = new Bundle();
//                b.putBoolean(SYNC_IDENTIFICATION_EXTRA + aName, true);
//                ContentResolver.addPeriodicSync(account, auth, b, 3600);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            newAccount = true;
//        }
//
//        if (newAccount || !setupCompleted) {
//            prefs.setSyncCompleted(true);
//        }
//
//        SBGenUtils.startSvc(context, src, true);
//
//    }

    @TargetApi(Build.VERSION_CODES.FROYO)
    public static void createPlayerSyncAccount(Context context) {

        mPreferences = PreferencesManager.getInstance(context);

        boolean newAccount = false;
        boolean setupCompleted = mPreferences.getSyncSetupCompleted();

        mAccountName = context.getResources().getString(R.string.app_name);
        mAccountType = context.getResources().getString(R.string.player_account_type);
        mContentAuthority =  mPreferences.getStubContentAuthority();

        // Create account, if it's missing. (Either first run, or user has deleted account.)
        Account account = SyncAccountService.getAccount(mAccountName, mAccountType);
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        if (accountManager.addAccountExplicitly(account, null, null) && !mContentAuthority.isEmpty()) {

            try {
                ContentResolver.setIsSyncable(account, mContentAuthority, 1);
                ContentResolver.setSyncAutomatically(account, mContentAuthority, true);

                Bundle b = new Bundle();
                b.putBoolean(SYNC_IDENTIFICATION_EXTRA + mAccountName, true);
                ContentResolver.addPeriodicSync(account, mContentAuthority, b, SYNC_FREQUENCY);

            } catch (Exception e) {
                e.printStackTrace();
            }
            newAccount = true;
        }

        if (newAccount || !setupCompleted) {
            mPreferences.setSyncSetupCompleted(true);
        }
    }
}
