package com.cowabunga.pepperradio.syncAdapter;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.util.Pair;
import android.util.Log;
import android.widget.RemoteViews;

import com.cowabunga.pepperradio.application.PreferencesManager;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Calendar;


import static android.content.Context.NOTIFICATION_SERVICE;

//import com.hydra.bpm.MPB;
//import com.hydra.bpm.MPBPreferences;

/**
 * Created by akis on 26/08/2016.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    private static Context mContext;

    /**
     * Constructor. Obtains handle to content resolver for later use.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContext = context;

    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {

        Log.d("SyncAdapter", "onPerformSync"); // todo o
//        PreferencesManager preferencesManager = PreferencesManager.getInstance(mContext);
//        Preferences mpbPreferences = Preferences.getInstance(mContext);
//        if (mpbPreferences.isBatMobileEnabled()) {
//            RadioPlayerUtils.callBatman(mContext, "SYNC");
//        }
//        int grLocalTime = RadioPlayerUtils.getLocalHour();
//
//        Calendar calendar = Calendar.getInstance();
//        int day = calendar.get(Calendar.DAY_OF_WEEK);
//        String weekDay = RadioPlayerUtils.getWeekDays().get(day - 1);
//
//        String favList = preferencesManager.getFavoriteProducersList();
//
//        if (favList.isEmpty())
//            return;
//
//        ArrayList<String> producersIds = new ArrayList<>();
//        try {
//            JSONArray jArray = new JSONArray(favList);
//            for (int i = 0; i < jArray.length(); i++) {
//                producersIds.add(jArray.getString(i));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        Pair<Boolean, String> upcomingEventPair = preferencesManager.scheduleHasUpcomingEvent(weekDay, String.valueOf(grLocalTime + 1), producersIds);
//
//        if (upcomingEventPair.first && preferencesManager.isEventNotificationsEnabled())
//            showNotification(mContext, upcomingEventPair.second);

    }

}
