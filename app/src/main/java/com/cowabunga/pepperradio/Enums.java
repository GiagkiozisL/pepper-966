package com.cowabunga.pepperradio;

import android.support.annotation.IntDef;
import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


public class Enums {

    // region //// Font slyle /////
    public static final int ITALIC = 0;
    public static final int LIGHT = 1;
    public static final int REGULAR = 2;
    public static final int BOLD = 3;
    public static final int BOLD_ITALIC = 4;
    public static final int THIN = 5;

    @IntDef({ITALIC, LIGHT, REGULAR, BOLD, BOLD_ITALIC, THIN})
    @Retention(RetentionPolicy.SOURCE)
    public @interface FontStyle {}
    // endregion //// Font style /////

    // region //// font type ////
    public static final String LATO = "lato";
    public static final String ROBOTO = "roboto";
    public static final String NOTO_SANS = "noto_sans";


    @StringDef({LATO, ROBOTO, NOTO_SANS})
    @Retention(RetentionPolicy.SOURCE)
    public @interface FontType {}

    // endregion //// font type ////
}
