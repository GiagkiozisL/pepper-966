package com.cowabunga.pepperradio.service;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;

import com.cowabunga.pepperradio.application.SBNotificationManager;


@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class SBJService extends JobService {


    private boolean cancel = false;
    private boolean itworks = false;
    private SBNotificationManager notificationManager;


    @Override
    public boolean onStartJob(JobParameters jobParameters) {

        notificationManager = SBNotificationManager.getInstance();
        notificationManager.createNotification(this, "SBJService", "Service is ON!");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent var3 = new Intent("track.my.install");
                sendBroadcast(var3);
            }
        }, 5000);

        itworks = true;


        return itworks;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        cancel = true;
        boolean needsReschedule = itworks;

        jobFinished(jobParameters, needsReschedule);


        return needsReschedule;
    }

}
