package com.cowabunga.pepperradio.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.cowabunga.pepperradio.R;
import com.cowabunga.pepperradio.application.PepperApplication;
import com.cowabunga.pepperradio.application.PreferencesManager;
import com.cowabunga.pepperradio.model.Producer;
import com.cowabunga.pepperradio.ui.MainActivity;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;




public class RadioService extends Service {

    private static final String TAG = "RadioService";
    public static final String START_PLAYER = "RadioPlayer.START_PLAYER";
    public static final String STOP_PLAYER = "RadioPlayer.STOP_PLAYER";


    private static final String NOTIFICATION_CHANNEL_ID = "926";
    public static final String START_VISUALIZER = "RadioPlayer.START_VISUALIZER";
    public static final String STOP_VISUALIZER = "RadioPlayer.STOP_VISUALIZER";
    private static final String CHANNEL_ID = "RadioService";
    private static final String CHANNEL_NAME = "Service";

    private NotificationManager mNotificationMnager;
    public static final String RADIO_STATION_EXTRA = "RadioStationExtra";
    public static final int MINUTE_LIMITATION = 50;

    private Map<String, Bitmap> producerBitmapMap = new HashMap<>();

    public int notificationId = 1337;

    private RemoteViews notRemoteView;
    private PreferencesManager preferencesManager;
    private Handler producerHandler = new Handler();

    private Context cnx;

    private PowerManager.WakeLock wakeLock;
    private PepperApplication application;

    private Notification liveStreamNotification;
    private NotificationCompat.Builder mBuilder;
    //    private static AACPlayer streamer;
    private SimpleExoPlayer exoPlayer;

    private BroadcastReceiver producerReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null && intent.hasExtra("producer")) {
                Producer producer = intent.getParcelableExtra("producer");
                createNotification(producer);
            }
        }
    };


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(TAG, "startcommand");

        if (intent != null && intent.getAction() != null) {
            String action = intent.getAction();
            Log.d(TAG, "startcommand action " + action);


            switch (action) {
                case START_PLAYER:
                    startExoPlayer();
                    break;
                case STOP_PLAYER:
                    stopExoPlayer();
                    break;
                case "action":
                    if (application.isPlaying()) {
                        stopExoPlayer();
                        sendActionToActivity("pause");
                    } else {
                        startExoPlayer();
                        sendActionToActivity("play");
                    }
                    break;
            }
        }

        return Service.START_NOT_STICKY;
    }

    private void sendActionToActivity(String action) {
        Intent intent = new Intent(MainActivity.PLAYER_ACTION);
        intent.putExtra("action", action);
        sendBroadcast(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "create");

        preferencesManager = PreferencesManager.getInstance(this);
        application = PepperApplication.getInstance();

        try {
            registerReceiver(producerReceiver, new IntentFilter("producer.object"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            createNotification(null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            startForeground(notificationId, liveStreamNotification);
        }

        cnx = this;

        if (!preferencesManager.isSyncAdapterScheduled()) {
            application.scheduleSyncAdapter();
            sendBroadcast(new Intent(MainActivity.SYNC_ADAPTER_STARTED));
        }

        scheduleProducerRefresh();
    }

    private void startExoPlayer() {
        try {
            AudioManager audioManager = (AudioManager)this.getSystemService(Context.AUDIO_SERVICE);
            audioManager.requestAudioFocus(new AudioManager.OnAudioFocusChangeListener() {
                @Override
                public void onAudioFocusChange(int focusChange) {

                }
            }, 3, 1);

            Uri uri = Uri.parse(preferencesManager.getStreamUrl());
            TrackSelector trackSelector = new DefaultTrackSelector();
            LoadControl loadControl = new DefaultLoadControl();

            if (exoPlayer == null) {
                exoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);
            }
            DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "exoplayer2example"), null);
            ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
            MediaSource audioSource = new ExtractorMediaSource(uri, dataSourceFactory, extractorsFactory, null, null);
            exoPlayer.addListener(exoEventListener);
            exoPlayer.prepare(audioSource);

            application.setPlaying(true);
            createNotification(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopExoPlayer() {

        if (exoPlayer != null) {
            exoPlayer.stop();
            application.setPlaying(false);
            createNotification(null);
        }
    }

    private void scheduleProducerRefresh() {
        Calendar calendar = Calendar.getInstance();
        int minute = calendar.get(Calendar.MINUTE);
        int interval = 61 - minute;

        producerHandler.postDelayed(hourlyRunnable, interval * 60000);
    }

    private Runnable hourlyRunnable = new Runnable() {
        @Override
        public void run() {
            cnx.sendBroadcast(new Intent(MainActivity.REFRESH_PRODUCER));
            producerHandler.postDelayed(this, 3600);
        }
    };

    public void createNotification(Producer producer) {
        if (notRemoteView == null) {
            notRemoteView = new RemoteViews(getPackageName(), R.layout.custom_notification);
        }

        if (producer == null) {
            producer = application.getNowPlayingProducer();
        }

        String title = producer.name;

        try {
            Bitmap bitmap = new GetBitmapFromUrl().execute(producer.imageUrl).get();
            notRemoteView.setImageViewBitmap(R.id.custom_notification_track_cover, bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        notRemoteView.setTextViewText(R.id.custom_notification_track_info, title);//mNowPlayingTrack.getmArtist() + " - " + mNowPlayingTrack.getmTitle());


        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);

        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder = new NotificationCompat.Builder(this);

        mNotificationMnager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Best Radio channel", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setSound(null, null);
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationMnager.createNotificationChannel(notificationChannel);
        }

        mBuilder.setSmallIcon(R.drawable.ic_sperm_notification3)
                .setTicker("")
                .setAutoCancel(true)
                .setContentIntent(pIntent)
                .setSound(null)
                .setContent(notRemoteView);

        int controleRsc = application.isPlaying() ? R.drawable.pause : R.drawable.play;
        notRemoteView.setImageViewResource(R.id.custom_notification_control_btn, controleRsc);

        Intent actionIntent = new Intent(this, RadioService.class);
        actionIntent.setAction("action");
        PendingIntent actionPendingIntent = PendingIntent.getService(this, 0, actionIntent, 0);

        notRemoteView.setOnClickPendingIntent(R.id.custom_notification_control_btn, actionPendingIntent);
        notRemoteView.setTextViewText(R.id.custom_notification_app_name, getString(R.string.app_name));

        liveStreamNotification = mBuilder.build();
        // Create Notification Manager
//        mNotificationMnager.notify(notifId, liveStreamNotification);
    }

    public void cancelNotification() {
        mNotificationMnager.cancel(notificationId);
    }

    private class GetBitmapFromUrl extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {

            String urlStr = params[0];
            try {
                URL url = new URL(urlStr);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                return BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.ic_launcher);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
        }
    }

    private ExoPlayer.EventListener exoEventListener = new ExoPlayer.EventListener() {
        @Override
        public void onTimelineChanged(Timeline timeline, Object manifest) {
            Log.i(TAG,"onTimelineChanged");
        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            Log.i(TAG,"onTracksChanged");
        }

        @Override
        public void onLoadingChanged(boolean isLoading) {
            Log.i(TAG,"onLoadingChanged");
        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            Log.i(TAG,"onPlayerStateChanged: playWhenReady = "+String.valueOf(playWhenReady)
                    +" playbackState = "+playbackState);
            switch (playbackState){
                case ExoPlayer.STATE_ENDED:
                    Log.d(TAG,"Playback ended!");
                    exoPlayer.seekTo(0);
                    break;
                case ExoPlayer.STATE_READY:
                    exoPlayer.setPlayWhenReady(true);
                    break;
                case ExoPlayer.STATE_BUFFERING:
                    Log.d(TAG,"Playback buffering!");
                    break;
                case ExoPlayer.STATE_IDLE:
                    Log.d(TAG,"ExoPlayer idle!");
                    break;
            }
        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
            Log.i(TAG,"onPlaybackError: "+error.getMessage());
        }

        @Override
        public void onPositionDiscontinuity() {
            Log.i(TAG,"onPositionDiscontinuity");
        }
    };
}
