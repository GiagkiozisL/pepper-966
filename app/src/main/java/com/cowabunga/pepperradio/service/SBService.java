package com.cowabunga.pepperradio.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.cowabunga.pepperradio.application.SBNotificationManager;


public class SBService extends Service {


    private Handler handler = new Handler();
    private SBNotificationManager notificationManager;

    @Override
    public void onCreate() {
        super.onCreate();

        notificationManager = SBNotificationManager.getInstance();
        notificationManager.createNotification(this, "SBService", "Service is ON!");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent var3 = new Intent("track.my.install");
                sendBroadcast(var3);
            }
        }, 5000);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
