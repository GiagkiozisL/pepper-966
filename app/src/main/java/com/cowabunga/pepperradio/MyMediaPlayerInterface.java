package com.cowabunga.pepperradio;

public interface MyMediaPlayerInterface {
    void onPrepared();
    void onCompletion();
    void onFailed();
}
