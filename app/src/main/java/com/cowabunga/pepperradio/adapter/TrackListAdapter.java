package com.cowabunga.pepperradio.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cowabunga.pepperradio.R;
import com.cowabunga.pepperradio.application.PreferencesManager;
import com.cowabunga.pepperradio.model.Track;
import com.cowabunga.pepperradio.view.CustomTextView;

import java.util.ArrayList;


public class TrackListAdapter extends ArrayAdapter<Track> {

    private Context mContext;
    private PreferencesManager mPreferencesManager;
    private ArrayList<Track> cachedTracks;

    public TrackListAdapter(Context context, int resource) {
        super(context, resource);
        mContext = context;
        mPreferencesManager = PreferencesManager.getInstance(mContext);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public Track getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        ViewHolder viewHolder = new ViewHolder();

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.row_tracklist, parent, false);

            viewHolder.artistTextView = view.findViewById(R.id.row_tracklist_artist_txt);
            viewHolder.titleTextView = view.findViewById(R.id.row_tracklist_title_txt);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        final Track track = getItem(position);

        if (track != null) {
            viewHolder.titleTextView.setText(track.title);
            viewHolder.artistTextView.setText(track.artist);
        }

        int reversePos = getCount() - position;
        float alpha = 0.0f;
        try {
            alpha = 1 - ((float)reversePos / 10);
            Log.d("adapter", "reverse pos " + reversePos + " with aplha: " + alpha);
        } catch (Exception e) {
            e.printStackTrace();
        }

        view.setAlpha(alpha);

        return view;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    private static class ViewHolder {
        CustomTextView artistTextView;
        CustomTextView titleTextView;
    }

}
