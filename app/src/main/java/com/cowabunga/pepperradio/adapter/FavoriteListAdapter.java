package com.cowabunga.pepperradio.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageButton;

import com.cowabunga.pepperradio.R;
import com.cowabunga.pepperradio.db.queries.PPDBQueries;
import com.cowabunga.pepperradio.model.Track;
import com.cowabunga.pepperradio.ui.FavoriteTracksActivity;
import com.cowabunga.pepperradio.view.CustomTextView;

import java.util.Calendar;
import java.util.Locale;

public class FavoriteListAdapter extends CursorAdapter {


    private Context mContext;
    private LayoutInflater mInflater;

    public FavoriteListAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        final View view = mInflater.inflate(R.layout.row_favorite_track, parent, false);

        final ViewHolder viewHolder = new ViewHolder();
        viewHolder.mTrackArtist =  view.findViewById(R.id.row_tracklist_artist_txt);
        viewHolder.mTrackTitle =  view.findViewById(R.id.row_tracklist_title_txt);
        viewHolder.favoriteImgBtn = view.findViewById(R.id.row_tracklist_favorite_img_btn);
        viewHolder.mTimestamp =  view.findViewById(R.id.row_tracklist_timestamp);

        view.setTag(viewHolder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final ViewHolder viewHolder = (ViewHolder) view.getTag();

        final Track mTrack = PPDBQueries.parseTrackFromCursor(cursor);
        viewHolder.mTrackTitle.setText(mTrack.title);
        viewHolder.mTrackArtist.setText(mTrack.artist);
        viewHolder.favoriteImgBtn.setOnClickListener(setupOnFavoriteClickListener(mTrack));
        viewHolder.mTimestamp.setText(convertTimestampToDate(mTrack.timestamp));
    }

    private String convertTimestampToDate(long timestamp) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timestamp * 1000L);
        return DateFormat.format("dd-MM-yyyy hh:mm:ss", cal).toString();
    }

    private class ViewHolder {
        ImageButton favoriteImgBtn;
        CustomTextView mTrackArtist;
        CustomTextView mTrackTitle;
        CustomTextView mTimestamp;
    }

    private View.OnClickListener setupOnFavoriteClickListener(final Track track) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Alert dialog
                new AlertDialog.Builder(mContext)
                        .setTitle("Delete entry")
                        .setMessage("Are you sure you want to remove from favorites?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                ((FavoriteTracksActivity) mContext).removeFavoriteTrack(track);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(R.drawable.ic_heart_broken_black_18dp)
                        .show();
            }
        };
    }
}
