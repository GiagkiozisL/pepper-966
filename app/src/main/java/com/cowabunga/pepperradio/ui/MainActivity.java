package com.cowabunga.pepperradio.ui;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.codemybrainsout.ratingdialog.RatingDialog;
import com.cowabunga.pepperradio.utils.JSONParser;
import com.cowabunga.pepperradio.R;
import com.cowabunga.pepperradio.adapter.TrackListAdapter;
import com.cowabunga.pepperradio.application.Constants;
import com.cowabunga.pepperradio.application.PepperApplication;
import com.cowabunga.pepperradio.application.PreferencesManager;
import com.cowabunga.pepperradio.db.PPDBOperations;
import com.cowabunga.pepperradio.db.queries.PPDBQueries;
import com.cowabunga.pepperradio.model.Producer;
import com.cowabunga.pepperradio.model.Track;
import com.cowabunga.pepperradio.service.RadioService;
import com.cowabunga.pepperradio.utils.RadioPlayerUtils;
import com.cowabunga.pepperradio.view.CustomPlayerImageButton;
import com.cowabunga.pepperradio.view.CustomTextView;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.yandex.metrica.YandexMetrica;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {


    public static final int PERMISSION_RECORD_AUDIO = 100;
    public static final String SYNC_ADAPTER_STARTED = "sync_adapter_started";
    public static final String REFRESH_PRODUCER = "refresh_producer";
    public static final String PLAYER_ACTION = "PLAYER_ACTION";

    private static final long usersAnimDuration = 5000;

    private PepperApplication application;
    private CustomPlayerImageButton playBtn;
    private PreferencesManager preferences;

    private CircleImageView producerImageView;
    private ListView trackListView;
    private TrackListAdapter trackListAdapter;

    private Producer currentProducer;
    private Track currentTrack;

    private CustomTextView producerTxtView;
    private CustomTextView artistTxtView;
    private CustomTextView trackTxtView;
    private CustomTextView onlineTxtView;
    private CheckBox favCheckBox;
    private FloatingActionButton favoritesBtn;
    private FloatingActionButton shareBtn;

    private Handler historyHandler;
    private Handler nowPlayingHandler;
    private Handler onlineUsersHandler;

    private long timestamp = 0L;
    private int nowReqCounter = 1;
    private int prevOnline = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        application = PepperApplication.getInstance();
        preferences = PreferencesManager.getInstance(MainActivity.this);

        playBtn = findViewById(R.id.main_play_img_btn);
        playBtn.setOnClickListener(setupPlayerOnClickListener());

        producerTxtView = findViewById(R.id.main_producer_name);
        producerImageView = findViewById(R.id.producer_image_view);
        producerImageView.setOnClickListener(setupOnProducerClickListener());

        artistTxtView = findViewById(R.id.main_track_artist);
        trackTxtView = findViewById(R.id.main_track_name);

        onlineTxtView = findViewById(R.id.main_online_users_txt);

        favCheckBox = findViewById(R.id.main_fav_checkbox);

        favoritesBtn = findViewById(R.id.main_favorites_btn);
        favoritesBtn.setOnClickListener(setupOnFavActivityListener());

        shareBtn = findViewById(R.id.main_share_btn);
        shareBtn.setOnClickListener(setupOnShareClickListener());

        historyHandler = new Handler();
        nowPlayingHandler = new Handler();
        onlineUsersHandler = new Handler();

        timestamp = System.currentTimeMillis();

        trackListView = findViewById(R.id.main_track_listview);
        disableScroll();
        trackListAdapter = new TrackListAdapter(MainActivity.this, R.id.main_track_listview);
        trackListView.setAdapter(trackListAdapter);

        addUserData();
        preferences.addLaunchCounter();

        handleRateDialog();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_RECORD_AUDIO:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // All good!
                    playBtn.performClick();
                } else {
                    Toast.makeText(MainActivity.this, "Needs audio permission!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        getProducerInfo();
        getHistoryTracks();
        startNowPlaying();
        playBtn.setPlaying(application.isPlaying());

        try {
            registerReceiver(playerActionReceiver, new IntentFilter(PLAYER_ACTION));
            registerReceiver(timeChangedBroadcastReceiver, new IntentFilter(REFRESH_PRODUCER));
            registerReceiver(syncAdapterStartedReceiver, new IntentFilter(SYNC_ADAPTER_STARTED));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            unregisterReceiver(playerActionReceiver);
            unregisterReceiver(timeChangedBroadcastReceiver);
            unregisterReceiver(syncAdapterStartedReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {

//        Intent serviceIntent = new Intent(MainActivity.this, RadioService.class);
//        if (Build.VERSION.SDK_INT >= 26) {
//            stopService(serviceIntent);
//        } else {
//            startService(serviceIntent);
//        }
        super.onDestroy();
    }

    private void addUserData() {
        if (preferences.getUserId().isEmpty()) {
            preferences.setuSerId(UUID.randomUUID().toString());
        }
    }

    private void handleRateDialog() {
        int appLunches = preferences.getLaunchesCounter();

        // i wanna show it only the 5th time of launch
        if (appLunches == 5) {
            sendEventRating();
            showDialog();
        }
    }

    private void startServiceWithPlayerAction(boolean isPlaying) {

        Intent serviceIntent = new Intent(MainActivity.this, RadioService.class);

        String streamUrl = preferences.getStreamUrl();
        if (streamUrl.isEmpty()) {
            Toast.makeText(MainActivity.this, "Stream url not found. Please try later", Toast.LENGTH_LONG).show();
            return;
        }

        if (!isPlaying) {
            serviceIntent.setAction(RadioService.START_PLAYER);
            serviceIntent.putExtra(RadioService.RADIO_STATION_EXTRA, preferences.getStreamUrl()); // + String.valueOf(System.currentTimeMillis()));
        } else {
            serviceIntent.setAction(RadioService.STOP_PLAYER);
        }

        if (Build.VERSION.SDK_INT >= 26) {
            startForegroundService(serviceIntent);
        } else {
            startService(serviceIntent);
        }

    }

    private void disableScroll() {
        trackListView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
    }

    private synchronized void saveIntoDB(List<Producer> producerList) {
        for (Producer dj : producerList) {
            PPDBOperations.insertProducerToDB(this, dj);
        }
    }

    private void getProducerInfo() {

        if (preferences.shouldFetchDjs(MainActivity.this)) {
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReference();
            StorageReference islandRef = storageRef.child("djs.json");
            long one_mega = 1024 * 1024;
            islandRef.getBytes(one_mega).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                public void onSuccess(byte[] bytes) {
                    String jsonStr = new String(bytes);

                    JSONParser.parseUrls(jsonStr, MainActivity.this);

                    List<Producer> producerList = JSONParser.parseProducersFormJson(jsonStr);
                    saveIntoDB(producerList);

                    loadCurrentProducer();
                }
            }).addOnFailureListener(new OnFailureListener() {
                public void onFailure(@NonNull Exception exception) {
                }
            });
        } else {
            loadCurrentProducer();
        }
    }

    private void getHistoryTracks() {
        try {
            new GethistoryAsyncTask().execute(preferences.getHistoryUrl());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCurrentProducer() {
        int hourInWeek = RadioPlayerUtils.findHourTimeInWeek();
        List<Producer> producerList = PPDBQueries.queryAllProducers(MainActivity.this);

        Producer currentProducer = null;
        for (Producer dj : producerList) {
            String workingHours = dj.workingHours;

            try {
                JSONArray whArry = new JSONArray(workingHours);
                for (int i = 0; i < whArry.length(); i++) {
                    JSONObject whObj = whArry.optJSONObject(i);
                    Iterator<String> iter = whObj.keys(); //This should be the iterator you want.
                    while (iter.hasNext()) {
                        String key = iter.next();
                        String val = (String)whObj.get(key);

                        int keyInt = Integer.parseInt(key);
                        int valInt = Integer.parseInt(val);

                        if (hourInWeek >= keyInt && hourInWeek < valInt) {
                            currentProducer = dj;
                            producerFound(currentProducer);
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (currentProducer == null) {
            Producer producer = new Producer();
            producer.id = Constants.empty_track_id;
            producer.name = "Pepper Radio";
            producer.onlineUsers = 50;
            producer.infoUrl = "https://www.pepper966.gr/";
            producer.imageUrl = "https://www.pepper966.gr/wp-content/uploads/2020/01/Pepper-radio-new-logo.jpg";
            producer.workingHours = "[]";

            producerFound(producer);
        }
    }

    private void producerFound(Producer producer) {

        Glide.with(MainActivity.this)
                .load(Uri.parse(producer.imageUrl))
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .centerCrop()
                .crossFade()
                .error(R.drawable.ic_pepper_logo)
                .into(producerImageView);

        producerTxtView.setText(producer.name);

        currentProducer = producer;
        application.setNowPlayingProducer(currentProducer);
        onlineTrick();
        sendProducerToService(currentProducer);
    }

    private void renderHistoryData(String historyData) {
        try {
            List<Track> currentTracks = new ArrayList<>();
            JSONArray jsonArray = new JSONArray(historyData);
            for (int i = 0; i < jsonArray.length();i++) {
                JSONObject jsonObject = jsonArray.optJSONObject(i);
                Track track = new Track();
                track.title = RadioPlayerUtils.formatCapsName(jsonObject.optString("track"));
                track.artist = RadioPlayerUtils.formatCapsName(jsonObject.optString("artist"));
                track.trackId = track.artist + "_" + track.title;
                track.album = RadioPlayerUtils.formatCapsName(jsonObject.optString("album"));
                track.duration = jsonObject.optInt("duration");
                track.startDate = jsonObject.optString("tm");

                long timePlayed = RadioPlayerUtils.getTimestampFromDate(track.startDate);
                if (timePlayed == -1) {
                    timePlayed = System.currentTimeMillis();
                }
                track.timestamp = timePlayed;
                currentTracks.add(track);
            }

            // sort
            Collections.sort(currentTracks, new Comparator<Track>() {
                @Override
                public int compare(Track var1, Track var2) {
                    return Long.compare(var1.timestamp, var2.timestamp);
                }
            });

            fillListView(currentTracks);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fillListView(List<Track> recentTracks) {
        if (recentTracks != null && recentTracks.size() > 0) {
            trackListAdapter.clear();
            trackListAdapter.addAll(recentTracks);
            trackListAdapter.notifyDataSetChanged();

            int count = trackListAdapter.getCount();
            Track lastTrack = trackListAdapter.getItem(count - 1);
            setupLastTrack(lastTrack);
        }
    }

    private void setupLastTrack(Track lastTrack) {
        try {
            if (lastTrack != null && !lastTrack.trackId.equalsIgnoreCase(Constants.empty_track_id)) {
                trackTxtView.setText(lastTrack.title);
                artistTxtView.setText(lastTrack.artist);

                currentTrack = lastTrack;

                favCheckBox.setOnCheckedChangeListener(null);
                favCheckBox.setChecked(PPDBQueries.trackIsFavorite(MainActivity.this, lastTrack.trackId));
                favCheckBox.setVisibility(View.VISIBLE);
                favCheckBox.setOnCheckedChangeListener(setupOnFavChangedListener());
            } else {
                favCheckBox.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            favCheckBox.setVisibility(View.GONE);
        }

        scheduleGetHistory();
    }

    private void scheduleGetHistory() {

        historyHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getHistoryTracks();
            }
        }, TimeUnit.MINUTES.toMillis(5));
    }

    private void startNowPlaying() {
        nowPlayingHandler.postDelayed(nowPlayingRunnable, TimeUnit.SECONDS.toMillis(1));
    }

    private void animateOnlineUsers(int currentUsers) {
        boolean increases = prevOnline < currentUsers;
        int diff = increases ? (currentUsers - prevOnline) : (prevOnline - currentUsers);
        if (diff == 0){
            return;
        }
        long frame = usersAnimDuration / diff;


        CountDownTimer timer = new CountDownTimer(usersAnimDuration, frame) {
            @Override
            public void onTick(long millisUntilFinished) {

                if (increases) {
                    prevOnline++;
                } else {
                    prevOnline--;
                }
                drawOnlineUsers(String.valueOf(prevOnline));
            }

            @Override
            public void onFinish() {

            }
        };
        timer.start();
    }

    private void drawOnlineUsers(String users) {

        SpannableStringBuilder builder = new SpannableStringBuilder();

        Spannable word = new SpannableString(users);
        word.setSpan(new ForegroundColorSpan(ContextCompat.getColor(MainActivity.this, R.color.white_text)), 0, word.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(word);
        Spannable wordTwo = new SpannableString(" online ");
        wordTwo.setSpan(new ForegroundColorSpan(ContextCompat.getColor(MainActivity.this, R.color.colorAccentTrans)), 0, wordTwo.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(wordTwo);
        Spannable wordThree = new SpannableString("streamers");
        wordThree.setSpan(new ForegroundColorSpan(ContextCompat.getColor(MainActivity.this, R.color.white_text)), 0, wordThree.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(wordThree);

        onlineTxtView.setText(builder);
    }

    private void sendProducerToService(Producer producer) {
        Intent intent = new Intent("producer.object");
        intent.putExtra("producer", producer);
        sendBroadcast(intent);
    }

    private void shareContent() {
        String trackDetails = "";
        if (currentTrack != null) {
            trackDetails = currentTrack.artist + "-" + currentTrack.title;
        }

        String sharingUrl = "https://play.google.com/store/apps/details?id=com.cowabunga.pepperradio";
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TEXT, "Listening now on Pepper 96.6 \n" + trackDetails + "\n Download app from " + sharingUrl);
        startActivity(Intent.createChooser(i, "Sharing Pepper"));
    }

    private void hideSharingInAWhile() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                shareBtn.hide();
            }
        }, 10000);
    }

    private void sendEventRating() {
        YandexMetrica.reportEvent(preferences.getUserId(), "Rating_Dialog_Showed");
    }

    private void showDialog() {

        final RatingDialog ratingDialog = new RatingDialog.Builder(this)
                .icon(ContextCompat.getDrawable(MainActivity.this, R.drawable.rate_icon))
                .ratingBarColor(R.color.main_pepperradio_color)
                .title("Do you like our app? Give us some boooost!")
                .playstoreUrl("https://play.google.com/store/apps/details?id=com.cowabunga.pepperradio")
                .onRatingBarFormSumbit(new RatingDialog.Builder.RatingDialogFormListener() {
                    @Override
                    public void onFormSubmitted(String feedback) {
                        Log.i("","Feedback:" + feedback);
                    }
                }).build();

        ratingDialog.show();
    }


    private Runnable nowPlayingRunnable = new Runnable() {
        @Override
        public void run() {

            try {
                String nowPlayingUrl = preferences.getNowPlayingUrl();

                String url = nowPlayingUrl + String.valueOf(timestamp) + "-=" + String.valueOf(timestamp + nowReqCounter);
                Log.d("nowplayrunn", "url: " + url);
                new GetNowPlayingAsyncTask().execute(url);
            } catch (Exception e) {
                e.printStackTrace();
            }

            nowPlayingHandler.postDelayed(this, TimeUnit.MINUTES.toMillis(1));
        }
    };

    private Runnable onlineUsersRunnable = new Runnable() {
        @Override
        public void run() {
//            Logger.d(TAG, "online trick calculates..");
            final String trimmeid = currentProducer.id.replaceAll("-", "");
            long totalUsers = 0;

            int onlineUsers = currentProducer.onlineUsers;

            Calendar c = Calendar.getInstance();
            int min = (60 - c.get(Calendar.MINUTE)) / 2;
            char cs = trimmeid.charAt(min);
            long cNum = Long.parseLong(String.valueOf(cs), 16);

            if ((cNum % 2) == 0) {
                totalUsers = onlineUsers + cNum;
            } else {
                totalUsers = onlineUsers - cNum;
            }

            animateOnlineUsers((int)totalUsers);


            onlineUsersHandler.postDelayed(this, 45000);
        }
    };


    /// region /// online users trick
    private void onlineTrick() {
//        Logger.d(TAG, "online trick starts!");

        int desireSec = 15;
        Calendar calendar = Calendar.getInstance();
        int sec = calendar.get(Calendar.SECOND);

        long delay;
        if (sec > desireSec) {
            delay = (60 - sec) + 15;
        } else {
            delay = desireSec - sec;
        }

        onlineUsersHandler.postDelayed(onlineUsersRunnable, delay * 1000);
    }

    /// endregion /// online users trick

    private BroadcastReceiver playerActionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null && intent.hasExtra("action")) {
                String action = intent.getStringExtra("action");

                playBtn.setPlaying(action.equalsIgnoreCase("play"));
            }
        }
    };

    private BroadcastReceiver timeChangedBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("MainActivity", "timeChangedBroadcastReceiver");
            getProducerInfo();
        }
    };

    private BroadcastReceiver syncAdapterStartedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            // todo tabsAdapter.setFavoriteBtnVisibility(View.VISIBLE);
        }
    };

    private View.OnClickListener setupPlayerOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT > 23 && ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSION_RECORD_AUDIO);
                    return;
                }

                boolean checkIsPlaying = !application.isPlaying();
                playBtn.setPlaying(checkIsPlaying);
                application.setPlaying(checkIsPlaying);

                startServiceWithPlayerAction(!checkIsPlaying);
            }
        };
    }

    private View.OnClickListener setupOnFavActivityListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent favIntent = new Intent(MainActivity.this, FavoriteTracksActivity.class);
                startActivity(favIntent);
            }
        };
    }

    private View.OnClickListener setupOnShareClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareContent();
            }
        };
    }

    private View.OnClickListener setupOnProducerClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (currentProducer != null && !currentProducer.infoUrl.isEmpty()) {
                    Intent webIntent = new Intent(MainActivity.this, WebViewActivity.class);
                    webIntent.putExtra("url", currentProducer.infoUrl);
                    startActivity(webIntent);
                }
            }
        };
    }

    private CheckBox.OnCheckedChangeListener setupOnFavChangedListener() {
        return new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    PPDBOperations.insertFavoriteTrackToDB(MainActivity.this, currentTrack);
                    Snackbar.make(buttonView, "Share with a friend", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    shareBtn.show();
                    hideSharingInAWhile();
                } else {
                    PPDBOperations.removeFavoriteTrackFromDB(MainActivity.this, currentTrack);
                    shareBtn.hide();
                }
            }
        };
    }


    private class GethistoryAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {

            String historyJsonStr = "{}";
            String downloadUrl = strings[0];
            try {
                URL url = new URL(downloadUrl);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setConnectTimeout(3000);

                int statusCode = c.getResponseCode();

                if (statusCode == 200) {
                    BufferedReader buffReader = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String output;
                    while ((output = buffReader.readLine()) != null) {
                        sb.append(output);
                    }
                    buffReader.close();

                    historyJsonStr = sb.toString();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return historyJsonStr;
        }

        @Override
        protected void onPostExecute(String historyJson) {
            super.onPostExecute(historyJson);

            renderHistoryData(historyJson);
        }
    }

    private class GetNowPlayingAsyncTask extends AsyncTask<String, Void, Track> {

        @Override
        protected Track doInBackground(String... strings) {
            Track track = new Track();
            String downloadUrl = strings[0];
            try {
                URL url = new URL(downloadUrl);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setConnectTimeout(3000);

                int statusCode = c.getResponseCode();

                if (statusCode == 200) {
                    BufferedReader buffReader = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String output;
                    while ((output = buffReader.readLine()) != null) {
                        sb.append(output);
                    }
                    buffReader.close();

                    track = JSONParser.parseTrackFromjs(sb.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return track;
        }

        @Override
        protected void onPostExecute(Track track) {
            super.onPostExecute(track);

            nowReqCounter++;
            setupLastTrack(track);
        }
    }
}
