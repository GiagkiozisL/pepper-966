package com.cowabunga.pepperradio.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.cowabunga.pepperradio.R;
import com.cowabunga.pepperradio.view.MyWebView;
import com.google.android.gms.flags.IFlagProvider;

public class WebViewActivity extends AppCompatActivity {


    private MyWebView webView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Producer Info");

        webView = findViewById(R.id.web_view);

        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("url")) {
            String url = intent.getStringExtra("url");
            webView.loadUrl(url);
        } else {
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
