package com.cowabunga.pepperradio.ui;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cowabunga.pepperradio.R;
import com.cowabunga.pepperradio.adapter.FavoriteListAdapter;
import com.cowabunga.pepperradio.db.PPContentProvider;
import com.cowabunga.pepperradio.db.PPDBOperations;
import com.cowabunga.pepperradio.db.tables.FavoriteTracksTable;
import com.cowabunga.pepperradio.model.Track;

public class FavoriteTracksActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int LOADER_ID = 5;

    private RelativeLayout emptyRelative;
    private FavoriteListAdapter favoriteListAdapter;
    private ListView listView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Favorites");

        emptyRelative = findViewById(R.id.activity_tracklist_empty_relative);
        listView = findViewById(R.id.activity_tracklist_listview);
        favoriteListAdapter = new FavoriteListAdapter(this, null, -1);
        listView.setAdapter(favoriteListAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getSupportLoaderManager().getLoader(LOADER_ID) != null) {
            getSupportLoaderManager().initLoader(LOADER_ID, null, this);
        } else {
            getSupportLoaderManager().restartLoader(LOADER_ID, null, this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void removeFavoriteTrack(Track track) {
        PPDBOperations.removeFavoriteTrackFromDB(FavoriteTracksActivity.this, track);
    }

    //// LoaderManager.LoaderCallbacks<Cursor>
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        Uri uri = PPContentProvider.CONTENT_URI_FAVORITE_TRACKS;
        String[] projection = PPContentProvider.FAVORITE_TRACK_PROJECTION;
        String sortOrder = FavoriteTracksTable.COLUMN_TIMESTAMP + " DESC";

        return new CursorLoader(FavoriteTracksActivity.this,
                uri,
                projection,
                null,
                null,
                sortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if (data != null && data.getCount() > 0) {
            favoriteListAdapter.swapCursor(data);
            emptyRelative.setVisibility(View.GONE);
        } else {
            emptyRelative.setVisibility(View.VISIBLE);
            Toast.makeText(FavoriteTracksActivity.this, "No data!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

}
