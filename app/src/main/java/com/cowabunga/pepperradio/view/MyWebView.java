package com.cowabunga.pepperradio.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

public class MyWebView extends WebView {

    public MyWebView(Context context) {
        super(context);
        init();
    }

    public MyWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        WebSettings webSettings = getSettings();

        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setAllowContentAccess(true);
        webSettings.setDatabaseEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setJavaScriptEnabled(true);
        CustomWebViewClient cwv = new CustomWebViewClient();
        setWebViewClient(cwv);
    }

    @Override
    public void loadUrl(String url) {
        super.loadUrl(url);
    }

    private class CustomWebViewClient extends WebViewClient {


        public CustomWebViewClient() {

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return false;
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(@NonNull WebView view, @NonNull String url) {

            if (Build.VERSION.SDK_INT > 20) {
                return null;
            }

            HttpURLConnection connection = null;
            InputStream stream = null;
            String contentType = "";
            String encoding = "UTF-8";
            try {
                URL url2 = new URL(url);
                connection = (HttpURLConnection) url2.openConnection();
                connection.setInstanceFollowRedirects(false);
                // add headers
                Map<String, String> existingHeaders = new HashMap<>();
                existingHeaders.put("Accept-Encoding", "gzip, deflate");

                for (Map.Entry entry : existingHeaders.entrySet()) {
                    connection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
                }

                int statusCode = connection.getResponseCode();

                // handle redirection
                while (connection != null && (statusCode == HttpURLConnection.HTTP_MOVED_TEMP || statusCode == HttpURLConnection.HTTP_MOVED_PERM)) {
                    connection = followRedirects(connection, existingHeaders);
                    if (connection !=null) {
                        statusCode = connection.getResponseCode();
                    }
                }

                if (connection == null ) {
                    connection = (HttpURLConnection)new URL("http://www.google.com").openConnection();
                    statusCode = connection.getResponseCode();
                }

                if (connection.getContentType() != null) {
                    contentType = connection.getContentType();
                    try {
                        if (contentType.contains(";")) {
                            contentType = contentType.split(";")[0];
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                encoding = connection.getContentEncoding();
                if ("gzip".equals(encoding)) {
                    stream = new GZIPInputStream(connection.getInputStream());
                } else {
                    if (statusCode != HttpURLConnection.HTTP_OK) {
                        stream = connection.getErrorStream();
                    } else {
                        stream = connection.getInputStream();
                    }
                }
                return new WebResourceResponse(contentType, encoding, stream);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return super.shouldInterceptRequest(view, url);
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {

            HttpURLConnection connection = null;
            InputStream stream = null;
            WebResourceResponse response =null;
            String contentType = "";
            String encoding = "UTF-8";
            String url = "";

            try {
                url = request.getUrl().toString();

                URL url_obj = new URL(url);
                connection = (HttpURLConnection)url_obj.openConnection();
                connection.setRequestMethod(request.getMethod());
                connection.setInstanceFollowRedirects(false);

                // add headers
                Map<String, String> existingHeaders = request.getRequestHeaders();

                existingHeaders.put("Accept-Encoding", "gzip, deflate");

                for (String key : existingHeaders.keySet()) {
                    connection.setRequestProperty(key, existingHeaders.get(key));
                }

                int statusCode = connection.getResponseCode();

                // handle redirection
                while (connection !=null && (statusCode == HttpURLConnection.HTTP_MOVED_TEMP || statusCode == HttpURLConnection.HTTP_MOVED_PERM)) {
                    connection = followRedirects(connection, existingHeaders);
                    if (connection != null) {
                        statusCode = connection.getResponseCode();
                    }
                }

                if (connection == null ) {
                    connection = (HttpURLConnection)new URL("http://www.google.com").openConnection();
                    statusCode = connection.getResponseCode();
                }

                if (connection.getContentType() != null) {
                    contentType = connection.getContentType();
                    try {
                        if (contentType.contains(";")) {
                            contentType = contentType.split(";")[0];
                        }
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }
                }

                encoding = connection.getContentEncoding();
                if ("gzip".equals(encoding)) {
                    stream = new GZIPInputStream(connection.getInputStream());
                } else {
                    if (statusCode != HttpURLConnection.HTTP_OK) {
                        stream = connection.getErrorStream();
                    } else {
                        stream = connection.getInputStream();
                    }
                }

                String reasonPhase = "OK";
                Map<String, String> responseHeaders = new HashMap<>();
                response = new WebResourceResponse(contentType, encoding, statusCode, reasonPhase, responseHeaders, stream);

                return response;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            }
            return super.shouldInterceptRequest(view, request);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        private HttpURLConnection followRedirects(HttpURLConnection connection, Map<String, String> extHeaders ) {
            try {
                String url = connection.getHeaderField("Location");

                String newUrl = java.net.URLDecoder.decode(url, "UTF-8");

                HttpURLConnection newcon = (HttpURLConnection)new URL(newUrl).openConnection();
                newcon.setInstanceFollowRedirects(false);
                newcon.setRequestMethod(connection.getRequestMethod());
                for (String key : extHeaders.keySet()) {
                    if (!key.equalsIgnoreCase("Upgrade-Insecure-Requests") && !key.equalsIgnoreCase("X-Requested-With")) {
                        newcon.setRequestProperty(key, extHeaders.get(key));
                    }
                }
                connection.getInputStream().close();
                connection.disconnect();
                return newcon;

            } catch (Exception e) {
//                e.printStackTrace();
            }
            return null;
        }
    }
}
