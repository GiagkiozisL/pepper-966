package com.cowabunga.pepperradio.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;
import android.widget.ImageButton;

import com.cowabunga.pepperradio.R;


public class CustomPlayerImageButton extends AppCompatImageButton {

    private static final int[] STATE_PLAY = {R.attr.state_play};

    private boolean mIsPlaying = false;

    public CustomPlayerImageButton(Context context) {
        super(context);
        initView(mIsPlaying);
    }

    public CustomPlayerImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CustomPlayerImageButton, 0, 0);
        try {
            mIsPlaying = typedArray.getBoolean(R.styleable.CustomPlayerImageButton_state_play, false);
        } finally {
            typedArray.recycle();
        }
        initView(mIsPlaying);
    }

    @Override
    public int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 2);
        if (!mIsPlaying) {
            mergeDrawableStates(drawableState, STATE_PLAY);
        }

        return drawableState;
    }

    public void setPlaying(boolean isPlaying) {
        mIsPlaying = isPlaying;
        initView(isPlaying);
    }

    private void initView(boolean isPlaying) {
        setImageResource(!isPlaying ? R.drawable.play2 : R.drawable.pause2);
    }

    public boolean isPlaying() {
        return mIsPlaying;
    }
}
