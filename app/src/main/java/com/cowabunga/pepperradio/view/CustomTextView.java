package com.cowabunga.pepperradio.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.cowabunga.pepperradio.Enums;
import com.cowabunga.pepperradio.R;


public class CustomTextView extends AppCompatTextView {


    private static final float DEFAULT_TEXT_SIZE = 14f;

    private Context mContext;
    private int mBackgroundColor;
    private int mBackgroundDrawable;
    private int mTextColor;
    private String mText;
    private float mTextSize;
    private int mFontTypeface = 0;
    private int mFontIndex = 2;

    private String[] monFontPaths = new String[]{"fonts/Montserrat-Italic.ttf",
            "fonts/Montserrat-Light.ttf",
            "fonts/Montserrat-Regular.ttf",
            "fonts/Montserrat-SemiBold.ttf",
            "fonts/Montserrat-SemiBold.ttf",
            "fonts/Montserrat-Thin.ttf"};
    private String[] robFontPaths = new String[]{"fonts/Roboto-Italic.ttf",
            "fonts/Roboto-Light.ttf",
            "fonts/Roboto-Regular.ttf",
            "fonts/Roboto-Bold.ttf",
            "fonts/Roboto-Bold.ttf",
            "fonts/Roboto-Thin.ttf"};

    private String[] latoFontPaths = new String[]{"fonts/Lato-Italic.ttf",
            "fonts/Lato-Light.ttf",
            "fonts/Lato-Regular.ttf",
            "fonts/Lato-Bold.ttf",
            "fonts/Bold-BoldItalic.ttf",
            "fonts/Lato-Thin.ttf"};

    private @Enums.FontStyle
    int mFontStyle = Enums.REGULAR;

    private @Enums.FontType
    String mFontType = Enums.NOTO_SANS;

    public CustomTextView(Context context) {
        super(context);
        mContext = context;
        initView();
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;

        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CustomTextView, 0, 0);
        try {
            mBackgroundColor = typedArray.getColor(R.styleable.CustomTextView_customBackgroundColor, ContextCompat.getColor(context, android.R.color.transparent));
            mBackgroundDrawable = typedArray.getResourceId(R.styleable.CustomTextView_customBackgroundDrawable, 0);
            mText = typedArray.getString(R.styleable.CustomTextView_customText);
            mTextColor = typedArray.getColor(R.styleable.CustomTextView_customTextColor, ContextCompat.getColor(context, android.R.color.black));
            mTextSize = typedArray.getFloat(R.styleable.CustomTextView_customTextSize, DEFAULT_TEXT_SIZE);
            mFontTypeface = typedArray.getInt(R.styleable.CustomTextView_customFontType, 0);
            mFontType = mFontTypeface == 0 ? Enums.NOTO_SANS : Enums.ROBOTO;
            mFontIndex = typedArray.getInt(R.styleable.CustomTextView_customFontStyle, mFontStyle);
        } finally {
            typedArray.recycle();
        }
        initView();
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    private void initView() {

        final Typeface customFont = getTypefaceStyle();
        setTypeface(customFont);

        setText(mText);
        setTextColor(mTextColor);
        setTextSize(mTextSize);

        if (mBackgroundDrawable != 0)
            setBackgroundResource(mBackgroundDrawable);

        if (mBackgroundColor != 0)
            setBackgroundColor(mBackgroundColor);
    }

    private Typeface getTypefaceStyle() {

        String typePath = "";
        switch (mFontType) {
            case Enums.ROBOTO:
                typePath = robFontPaths[mFontIndex];
                break;
            case Enums.LATO:
                typePath = latoFontPaths[mFontIndex];
                break;
            case Enums.NOTO_SANS:
                typePath = latoFontPaths[mFontIndex];
                break;
        }

        if (typePath.isEmpty()) // if STILLS empty
            typePath = latoFontPaths[2];

        return Typeface.createFromAsset(mContext.getAssets(), typePath);

    }

}
