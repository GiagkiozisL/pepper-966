package com.cowabunga.pepperradio.application;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.util.Pair;


import com.cowabunga.pepperradio.db.PPDBOperations;
import com.cowabunga.pepperradio.model.Producer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class PreferencesManager {

    private static PreferencesManager sInstance;
    private final SharedPreferences mPref;

    private static final String PREF_NAME = "cowabunga.best926.player.PREF_NAME";
    private static final String CACHED_PRODUCERS_BITMAPS = "cowabunga.best926.player.CACHED_PRODUCERS_BITMAPS";
    private static final String CACHED_FAVORITE_TRACKS = "cowabunga.best926.player.CACHED_FAVORITE_TRACKS";
    private static final String FIRST_TIME_OPENED = "cowabunga.best926.player.FIRST_TIME_OPENED";
    private static final String SUBSCRIBED_TO_TOPICS = "cowabunga.best926.player.SUBSCRIBED_TO_TOPICS";
    private static final String TEMPORARY_NOW_TRACK_LIST = "cowabunga.best926.player.TEMPORARY_NOW_TRACK_LIST";
    private static final String SYNC_SETUP_COMPLETED = "cowabunga.best926.player.SYNC_SETUP_COMPLETED";
    private static final String STUB_CONTENT_AUTHORITY = "cowabunga.best926.player.STUB_CONTENT_AUTHORITY";
    private static final String SYNC_ADAPTER_SCHEDULED = "cowabunga.best926.player.SYNC_ADAPTER_SCHEDULED";
    private static final String VISUALIZER_ENABLED = "cowabunga.best926.player.VISUALIZER_ENABLED";
    private static final String FAVORITE_PRODUCER_LIST = "cowabunga.best926.player.FAVORITE_PRODUCER_LIST";
    private static final String FAV_GUIDE_APPEARED = "cowabunga.best926.player.FAV_GUIDE_APPEARED";
    private static final String SCHEDULE_HASHMAP = "cowabunga.best926.player.SCHEDULE_HASHMAP";
    private static final String EVENT_NOTIFICATIONS_ENABLED = "cowabunga.best926.player.EVENT_NOTIFICATIONS_ENABLED";
    private static final String USER_RATED = "cowabunga.best926.player.USER_RATED";
    private static final String DEVICE_ID = "cowabunga.best926.player.DEVICE_ID";
    private static final String CHAT_ENABLED_BY_USER = "cowabunga.best926.player.CHAT_ENABLED_BY_USER";
    private static final String STREAM_URL = "stream_url";
    private static final String crypt_enabled = "crypt_enabled";
    private static final String producer = "producer";
    private static final String display_name = "display_name";
    private static final String last_fetch_djs = "last_fetch_djs";
    private static final String history_url = "history_url";
    private static final String now_playing_url = "now_playing_url";
    private static final String launches_counter = "launches_counter";
    private static final String user_identifier = "user_identifier";

    private static long day_in_millis = TimeUnit.DAYS.toMillis(1);

    private PreferencesManager(Context context) {
        mPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized PreferencesManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PreferencesManager(context);
        }
        return sInstance;
    }

    public void setuSerId(String value) {
        mPref.edit().putString(user_identifier, value).apply();
    }

    public String getUserId() {
        return mPref.getString(user_identifier, "");
    }

    public void addLaunchCounter() {
        mPref.edit().putInt(launches_counter, getLaunchesCounter() + 1).apply();
    }

    public int getLaunchesCounter() {
        return mPref.getInt(launches_counter, 0);
    }

    public void setNowPlayingUrl(String value) {
        if (value == null || value.isEmpty()) {
            return;
        }
        mPref.edit().putString(now_playing_url, value).apply();
    }

    public String getNowPlayingUrl() {
        return mPref.getString(now_playing_url, "https://now-dot-playing-dot-radiojarcom.appspot.com/api/stations/pepper/now_playing/?callback=jQuery1101033503685218282575_"); //todo
    }

    public void setHistoryUrl(String value) {
        if (value == null || value.isEmpty()) {
            return;
        }
        mPref.edit().putString(history_url, value).apply();
    }

    public String getHistoryUrl() {
        return mPref.getString(history_url, "https://www.radiojar.com/api/stations/pepper/tracks/");
    }

    public void setLastFetchDj(long value) {
        mPref.edit().putLong(last_fetch_djs, value).apply();
    }

    public long getLastFetchDj() {
        return mPref.getLong(last_fetch_djs, 0L);
    }

    public boolean shouldFetchDjs(Context ctx) {
        long lastFetched = getLastFetchDj();
        long now = System.currentTimeMillis();
        if (lastFetched == 0L) {
            setLastFetchDj(now);
            return true;
        }

        if (now - lastFetched > day_in_millis) {
            PPDBOperations.dropProducers(ctx);
            setLastFetchDj(now);
            return true;
        }

        return false;
    }

    // region ////  CACHED_PRODUCERS_BITMAPS ////
    public void setCachedBitmaps(String producerName, String encodedBitmap) {

        HashMap<String, String> cachedProducerBitmaps = getCachedBitmaps();
        if (!cachedProducerBitmaps.containsKey(producerName)) {

            cachedProducerBitmaps.put(producerName, encodedBitmap);

            JSONArray jsonArray = new JSONArray();
            try {
                for (Map.Entry<String, String> entry : cachedProducerBitmaps.entrySet()) {

                    JSONObject object = new JSONObject();
                    String key = entry.getKey();
                    String value = entry.getValue();
                    object.put(key, value);

                    jsonArray.put(object);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            mPref.edit().putString(CACHED_PRODUCERS_BITMAPS, jsonArray.toString()).apply();
        }
    }

    public HashMap<String, String> getCachedBitmaps() {
        HashMap<String, String> cachedProducerBitmaps = new HashMap<>();

        String jsonArray = mPref.getString(CACHED_PRODUCERS_BITMAPS, "");
        if (!jsonArray.isEmpty()) {

            try {
                JSONArray array = new JSONArray(jsonArray);
                for (int i=0;i<array.length();i++) {
                    JSONObject obj = array.getJSONObject(i);

                    Iterator it = obj.keys();
                    while (it.hasNext()) {
                        String key = (String) it.next();
                        String value = obj.getString(key);
                        cachedProducerBitmaps.put(key, value);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return cachedProducerBitmaps;
    }

    public void resetCachedBitmaps() {
        mPref.edit().putString(CACHED_PRODUCERS_BITMAPS, "").apply();
    }
    // endregion ////  CACHED_PRODUCERS_BITMAPS ////

    // region //// FIRST_TIME_OPENED ////
    public void setFirstTimeOpened(boolean value) {
        mPref.edit().putBoolean(FIRST_TIME_OPENED, value).apply();
    }

    public boolean isFirstTimeOpened() {
        return mPref.getBoolean(FIRST_TIME_OPENED, true);
    }
    // endregion //// FIRST_TIME_OPENED ////

    // region //// SUBSCRIBED_TO_TOPICS ////
    public void setSubscribedToTopics(String value) {
        ArrayList<String> subcibedTopics = getSubscribedToTopics();
        JSONArray jsonArray = new JSONArray();

        if (subcibedTopics != null) {
            for (int i = 0; i< subcibedTopics.size(); i++) {
                jsonArray.put(subcibedTopics.get(i));
            }
            jsonArray.put(value);
        }

        mPref.edit().putString(SUBSCRIBED_TO_TOPICS, jsonArray.toString()).apply();
    }

    public ArrayList<String> getSubscribedToTopics() {

        ArrayList<String> subcibedTopics = new ArrayList<>();
        String stringArray = mPref.getString(SUBSCRIBED_TO_TOPICS, "");

        if (!stringArray.isEmpty()) {
            try {
                JSONArray jsonArray = new JSONArray(stringArray);
                if (jsonArray != null) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        String item = jsonArray.get(i).toString();
                        subcibedTopics.add(item);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return subcibedTopics;
    }

    public void resetSubsrcibedToTopics() {
        mPref.edit().putString(SUBSCRIBED_TO_TOPICS, "").apply();
    }
    // endregion //// SUBSCRIBED_TO_TOPICS ////

    // region //// SYNC_SETUP_COMPLETED ////
    public void setSyncSetupCompleted(boolean value) {
        mPref.edit().putBoolean(SYNC_SETUP_COMPLETED, value).apply();
    }

    public boolean getSyncSetupCompleted() {
        return mPref.getBoolean(SYNC_SETUP_COMPLETED, false);
    }
    // endregion //// SYNC_SETUP_COMPLETED ////

    // region //// STUB_CONTENT_AUTHORITY ////
    public void setStubContentAuthority(String value) {
        mPref.edit().putString(STUB_CONTENT_AUTHORITY, value).apply();
    }

    public String getStubContentAuthority() {
        return mPref.getString(STUB_CONTENT_AUTHORITY, "");
    }
    // endregion //// STUB_CONTENT_AUTHORITY ////

    // region //// SYNC_ADAPTER_SCHEDULED ////
    public void setSyncAdapterScheduled(boolean value) {
        mPref.edit().putBoolean(SYNC_ADAPTER_SCHEDULED, value).apply();
    }

    public boolean isSyncAdapterScheduled() {
        return mPref.getBoolean(SYNC_ADAPTER_SCHEDULED, false);
    }

    // endregion //// SYNC_ADAPTER_SCHEDULED ////

    // region //// VISUALIZER_ENABLED ////
    public void setVisualizerEnabled(boolean value) {
        mPref.edit().putBoolean(VISUALIZER_ENABLED, value).apply();
    }

    public boolean isVisualizerEnabled() {
        return mPref.getBoolean(VISUALIZER_ENABLED, false);
    }

    // endregion //// VISUALIZER_ENABLED ////

    // region //// FAVORITE_PRODUCER_LIST ////
    public void addProducerToFavoriteList(String value) {

        String curFavProdStr = getFavoriteProducersList();
        JSONArray jsonArray = new JSONArray();

        if (curFavProdStr.isEmpty()) {
            jsonArray.put(value);
        } else {
            try {
                jsonArray = new JSONArray(curFavProdStr);
                if (jsonArray.length() > 0) {
                    boolean producerExists = false;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        String producerId = (String) jsonArray.get(i);
                        producerExists = producerId.equalsIgnoreCase(value);
                    }
                    if (!producerExists)
                        jsonArray.put(value);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mPref.edit().putString(FAVORITE_PRODUCER_LIST, jsonArray.toString()).apply();
    }

    public String getFavoriteProducersList() {
        return mPref.getString(FAVORITE_PRODUCER_LIST, "");
    }

    public boolean isProducerInFavList(String value) {

        boolean producerExists = false;
        String curFavorites = getFavoriteProducersList();
        if (curFavorites.isEmpty())
            return false;

        try {
            JSONArray jsonArray = new JSONArray(curFavorites);

            for (int i = 0; i < jsonArray.length(); i++) {
                String producerId = (String) jsonArray.get(i);
                if (producerId.equalsIgnoreCase(value)) {
                    producerExists = true;
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return producerExists;
    }


    public void removeProducerFromFavorites(String value) {
        String curFavProdStr = getFavoriteProducersList();

        // should never access here
        if (curFavProdStr.isEmpty())
            return;

        try {
            JSONArray updatedArray = new JSONArray();
            JSONArray jsonArray = new JSONArray(curFavProdStr);
            if (jsonArray.length() > 0) {

                for (int i = 0; i < jsonArray.length(); i++) {
                    String producerId = (String) jsonArray.get(i);
                    if (!producerId.equalsIgnoreCase(value)) {
                        updatedArray.put(producerId);
                    }
                }
            }

            mPref.edit().putString(FAVORITE_PRODUCER_LIST, updatedArray.toString()).apply();
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    // endregion //// FAVORITE_PRODUCER_LIST ////

    // region //// FAV_GUIDE_APPEARED ////
    public void setShowcaseAppeared(boolean value) {
        mPref.edit().putBoolean(FAV_GUIDE_APPEARED, value).apply();
    }

    public boolean isShowcaseAppeared() {
        return mPref.getBoolean(FAV_GUIDE_APPEARED, false);
    }


    // endregion //// FAV_GUIDE_APPEARED ////

    // region //// EVENT_NOTIFICATIONS_ENABLED ////
    public void setEventNotificationsEnaled(boolean value) {
        mPref.edit().putBoolean(EVENT_NOTIFICATIONS_ENABLED, value).apply();
    }

    public boolean isEventNotificationsEnabled() {
        return mPref.getBoolean(EVENT_NOTIFICATIONS_ENABLED, true);
    }
    // endregion //// EVENT_NOTIFICATIONS_ENABLED ////

    // region //// USER_RATED ////
    public void setUserRated(boolean value) {
        mPref.edit().putBoolean(USER_RATED, value).apply();
    }

    public boolean hasUserRated() {
        return mPref.getBoolean(USER_RATED, false);
    }

    // endregion //// USER_RATED ////

    // region //// DEVICE_ID ////
    public void setDeviceId(String value) {
        mPref.edit().putString(DEVICE_ID, value).apply();
    }

    public String getDeviceId() {
        return mPref.getString(DEVICE_ID, "");
    }
    // endregion //// DEVICE_ID ////

    // region //// CHAT_ENABLED_BY_USER ////
    public void setChatEnabledByUser(boolean value) {
        mPref.edit().putBoolean(CHAT_ENABLED_BY_USER, value).apply();
    }

    public boolean isChatEnabledByUser() {
        return mPref.getBoolean(CHAT_ENABLED_BY_USER, false);
    }

    // endregion //// CHAT_ENABLED_BY_USER ////

    public void setStreamUrl(String value) {
        if (value == null || value.isEmpty()) {
            return;
        }
        mPref.edit().putString(STREAM_URL, value).apply();
    }

    public String getStreamUrl() {
        return mPref.getString(STREAM_URL, "https://n0d.radiojar.com/pepper.m4a?1583748252=&rj-tok=AAABcL7Kg6AA-hcLoK6iOif0dQ&rj-ttl=5"); // todo replace
    }

    public void setCryptEnabled(boolean value) {
        mPref.edit().putBoolean(crypt_enabled, value).apply();
    }

    public boolean isCryptEnabled() {
        return mPref.getBoolean(crypt_enabled, false);
    }

    public void setProducer(boolean iAmProducer) {
        mPref.edit().putBoolean(producer, iAmProducer).apply();
    }

    public boolean getProducer() {
        return mPref.getBoolean(producer, false);
    }

    public void setDisplay_name(String value) {
        mPref.edit().putString(display_name, value).apply();
    }

    public String getDisplay_name() {
        return mPref.getString(display_name, "");
    }

    // region //// SCHEDULE_HASHMAP ////
    public void setScheduleHashMap(Map<Pair<String, Integer>, Producer> hashMap) {

        try {

            JSONObject jsonObject = new JSONObject();

            for (Map.Entry<Pair<String, Integer>, Producer> entry : hashMap.entrySet()) {

                Pair<String, Integer> pair = entry.getKey();

                JSONArray dayArray;
                String dayKey = pair.first;

                if (jsonObject.has(dayKey)) {
                    dayArray = jsonObject.optJSONArray(dayKey);
                } else {
                    dayArray = new JSONArray();
                }

                JSONObject detObj = new JSONObject();
                detObj.put("time", pair.second);
                detObj.put("producer_name", entry.getValue().name);
                detObj.put("producer_id", entry.getValue().id);

                dayArray.put(detObj);
                jsonObject.put(dayKey, dayArray);

            }
//            Logger.d("PreferencesManager", jsonObject.toString());
            mPref.edit().putString(SCHEDULE_HASHMAP, jsonObject.toString()).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void resetScheduleHashMap() {
        mPref.edit().putString(SCHEDULE_HASHMAP, "").apply();
    }

    public Pair<Boolean, String> scheduleHasUpcomingEvent(String day, String startTime, ArrayList<String> producerIds) {
        boolean hasUpcomingEvent = false;
        String producerName = "";

        String scheduleStr = mPref.getString(SCHEDULE_HASHMAP, "");
        if (scheduleStr.isEmpty() || producerIds.isEmpty())
            return Pair.create(false, "");

        try {
            JSONObject jsonObject = new JSONObject(scheduleStr);

            if (jsonObject.has(day)) {

                JSONArray dayArray = jsonObject.optJSONArray(day);

                for (int i = 0; i < dayArray.length(); i++) {
                    JSONObject detailsObj = dayArray.optJSONObject(i);
                    String time = detailsObj.optString("time");
                    String prodName = detailsObj.optString("producer_name");
                    String prodId = detailsObj.optString("producer_id");

                    if (time.equalsIgnoreCase(startTime) && producerIds.contains(prodId)) {
                        hasUpcomingEvent = true;
                        producerName = prodName;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Pair.create(hasUpcomingEvent, producerName);
    }
    // endregion //// SCHEDULE_HASHMAP ////
}
