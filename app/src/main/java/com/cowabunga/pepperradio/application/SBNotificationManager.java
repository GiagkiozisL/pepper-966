package com.cowabunga.pepperradio.application;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;


import com.cowabunga.pepperradio.service.SBService;

import java.util.Random;

public class SBNotificationManager {

    private static final String CHANNEL_ID = "NManagerChannel";
    private static final String CHANNEL_NAME = "Oct";
    private static SBNotificationManager nManager;
    private boolean silence = true;

    public static SBNotificationManager getInstance() {
        if (nManager == null) {
            nManager = new SBNotificationManager();
        }
        return nManager;
    }

    private SBNotificationManager() {}

    public void createNotification(Context ctx, String title, String message) {

        if (silence) {
            return;
        }

        Intent resultIntent = new Intent(ctx, SBService.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(ctx, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManager mNotificationManager;
        NotificationCompat.Builder mBuilder;
        mBuilder = new NotificationCompat.Builder(ctx);
        mBuilder.setSmallIcon(android.R.drawable.stat_notify_sync);
        mBuilder.setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(false)
                .setDefaults(Notification.DEFAULT_ALL)
                .setSound(null)
                .setContentIntent(resultPendingIntent);

        mNotificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(false);
            assert mNotificationManager != null;
            mBuilder.setChannelId(CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }

        if (mNotificationManager != null)
            mNotificationManager.notify(new Random().nextInt() /* Request Code */, mBuilder.build());
    }

}
