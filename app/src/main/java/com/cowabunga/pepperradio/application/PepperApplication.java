package com.cowabunga.pepperradio.application;

import android.app.Application;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;

import com.cowabunga.pepperradio.model.Producer;
import com.cowabunga.pepperradio.syncAdapter.SyncUtils;
import com.google.firebase.FirebaseApp;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

import java.util.Calendar;

import static com.cowabunga.pepperradio.service.RadioService.MINUTE_LIMITATION;

public class PepperApplication extends Application {

    private boolean playing;
    private static PepperApplication instance;
    public MediaPlayer mMediaPlayer;
    private Producer currentProducer;

    private static final String yandex_key = "a073aa73-448e-48fe-9557-079dc6aba62d";


    public static PepperApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseApp.initializeApp(this);
        instance = this;

        initYandex();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public boolean isPlaying() {
        return playing;
    }

    public void setPlaying(boolean playing) {
        this.playing = playing;
    }

    public static void scheduleSyncAdapter() {
        Calendar c = Calendar.getInstance();
        int minutes = c.get(Calendar.MINUTE);
        int intervalTime = 1;
        if (minutes > MINUTE_LIMITATION) {
            intervalTime = (minutes - 60) + MINUTE_LIMITATION;// 0..50-60
        } else {
            intervalTime = MINUTE_LIMITATION - minutes;
        }

        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                PreferencesManager.getInstance(instance).setSyncAdapterScheduled(true);
                SyncUtils.createPlayerSyncAccount(instance);
            }
        }); //, intervalTime * 60 * 1000);

        Log.d("app", "sync scheduled in " + (intervalTime * 60 * 1000) + " milliseconds");
    }

    public Producer getNowPlayingProducer() {
        return currentProducer;
    }

    public void setNowPlayingProducer(Producer currentProducer) {
        this.currentProducer = currentProducer;
    }

    private void initYandex() {
        // Creating an extended library configuration.
        YandexMetricaConfig config = YandexMetricaConfig.newConfigBuilder(yandex_key).build();
        // Initializing the AppMetrica SDK.
        YandexMetrica.activate(getApplicationContext(), config);
        // Automatic tracking of user activity.
        YandexMetrica.enableActivityAutoTracking(this);
    }

}
